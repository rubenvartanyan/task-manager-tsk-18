package ru.vartanyan.tm.api.entity;

import java.util.Date;

public interface IHasDateStarted {

    Date getDateStarted();

    void setDateStarted(Date dateStart);

}
