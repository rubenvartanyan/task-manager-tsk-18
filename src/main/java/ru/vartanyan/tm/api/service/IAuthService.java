package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.model.User;

public interface IAuthService {

    public User getUser() throws Exception;

    public String getUserId() throws Exception;

    public boolean isAuth();

    public void logout();

    public void login(final String login, final String password) throws Exception;

    public void registry(final String login,
                         final String password,
                         final String email) throws Exception;

}
