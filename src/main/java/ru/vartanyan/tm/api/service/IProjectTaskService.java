package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    // SHOW ALL TASKS FROM PROJECT
    List<Task> findAllTaskByProjectId(String projectId) throws Exception;

    // ADD TASK TO PROJECT
    Task bindTaskByProjectId(String projectId, String taskId) throws Exception;

    // REMOVE TASK FROM PROJECT
    Task unbindTaskFromProject(String projectId, String taskId) throws EmptyIdException, Exception;

    // REMOVE ALL TASKS FROM PROJECT AND THEN PROJECT
    Project removeProjectById(String projectId) throws Exception;

}
