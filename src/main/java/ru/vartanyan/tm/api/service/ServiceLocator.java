package ru.vartanyan.tm.api.service;

public interface ServiceLocator {

    IUserService getUserService();

    IAuthService getAuthService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

    IProjectService getProjectService();

    ICommandService getCommandService();

}
