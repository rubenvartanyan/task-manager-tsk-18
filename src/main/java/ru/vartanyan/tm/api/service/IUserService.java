package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User findById(final String Id) throws Exception;

    User findByLogin(final String login) throws Exception;

    User removeUser(final User user);

    User removeById(final String Id) throws Exception;

    User removeByLogin(final String login) throws Exception;

    User create(final String login,
                final String password) throws Exception;

    User create(final String login,
                final String password,
                final String email) throws Exception;

    User create(final String login,
                final String password,
                final Role role) throws Exception;

    User setPassword(final String userId, final String password) throws Exception;

    User updateUser(final String userId,
                    final String firstName,
                    final String lastName,
                    final String middleName) throws Exception;

}
