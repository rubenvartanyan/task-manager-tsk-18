package ru.vartanyan.tm.api.repository;

import ru.vartanyan.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public interface IUserRepository {

    public List<User> findAll();

    public User add(final User user);

    public User findById(final String Id);

    public User removeUser(final User user);

    public User findByLogin(final String login);

    public User removeById(final String Id);

    public User removeByLogin(final String login);

}
