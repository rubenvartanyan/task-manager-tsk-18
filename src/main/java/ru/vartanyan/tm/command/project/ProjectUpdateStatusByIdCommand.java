package ru.vartanyan.tm.command.project;

import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectUpdateStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-status-update-by-id";
    }

    @Override
    public String description() {
        return "Update project status by Id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT STATUS]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = serviceLocator.getProjectService().updateProjectStatusById(id, status);
        System.out.println("[PROJECT STATUS UPDATED]");
    }

}
