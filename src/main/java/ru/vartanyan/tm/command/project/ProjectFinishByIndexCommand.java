package ru.vartanyan.tm.command.project;

import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectFinishByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-finish-by-index";
    }

    @Override
    public String description() {
        return "Finish project by index";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FINISH PROJECT]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().finishProjectByIndex(index);
        System.out.println("[PROJECT FINISHED]");
    }

}
