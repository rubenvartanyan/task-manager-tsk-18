package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User add(User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(String Id) {
        for (final User user: users) {
            if (user.getId().equals(Id)) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(String login) {
        for (final User user: users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    @Override
    public User removeUser(User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User removeById(String Id) {
        final User user = findById(Id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User removeByLogin(String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

}
