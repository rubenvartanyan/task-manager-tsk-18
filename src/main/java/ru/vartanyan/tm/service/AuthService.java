package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.service.IAuthService;
import ru.vartanyan.tm.api.service.IUserService;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.empty.EmptyPasswordException;
import ru.vartanyan.tm.exception.system.AccessDeniedException;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;
    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }


    @Override
    public User getUser() throws Exception {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public String getUserId() throws Exception {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(String login, String password) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(String login, String password, String email) throws Exception {
        userService.create(login, password, email);
    }

}
