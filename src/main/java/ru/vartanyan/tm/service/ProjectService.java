package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.service.IProjectService;
import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.exception.empty.EmptyDescriptionException;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyNameException;
import ru.vartanyan.tm.exception.incorrect.IncorrectIndexException;
import ru.vartanyan.tm.exception.system.NullProjectException;
import ru.vartanyan.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project findOneById(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        Project project = projectRepository.findOneById(id);
        if (project == null) throw new NullProjectException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(final Integer index) throws Exception {
       if (index == null || index < 0) throw new IncorrectIndexException(index);
       Project project = projectRepository.findOneByIndex(index);
       if (project == null) throw new NullProjectException();
       return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = projectRepository.findOneByName(name);
        if (project == null) throw new NullProjectException();
        return project;
    }

    @Override
    public Project add(final Project project) {
       return projectRepository.add(project);
    }

    @Override
    public Project remove(Project project) {
        return projectRepository.remove(project);
    }

    @Override
    public Project removeOneById(final String id) throws Exception {
       if (id == null || id.isEmpty()) throw new EmptyIdException();
        Project project = projectRepository.findOneById(id);
        return projectRepository.remove(project);
    }

    @Override
    public Project removeOneByIndex(final Integer index) throws Exception{
       if (index < 0) throw new IncorrectIndexException(index);
        Project project = projectRepository.findOneByIndex(index);
        if (project == null) throw new NullProjectException();
        return projectRepository.remove(project);
    }

    @Override
    public Project removeOneByName(final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        Project project = projectRepository.findOneByName(name);
        if (project == null) throw new NullProjectException();
        return projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project add(String name, String description) throws Exception {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public Project updateProjectById(final String id,
                                     final String name,
                                     final String description) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final Integer index,
                                        final String name,
                                        final String description) throws Exception {
        final Project project = findOneByIndex(index);
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(String id) throws Exception {
        final Project project = findOneById(id);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(String name) throws Exception {
        final Project project = findOneByName(name);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        project.setDateStarted(dateStarted);
        return project;
    }

    @Override
    public Project startProjectByIndex(Integer index) throws Exception {
        final Project project = findOneByIndex(index);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.IN_PROGRESS);
        Date dateStarted = new Date();
        project.setDateStarted(dateStarted);
        return project;
    }

    @Override
    public Project finishProjectById(String id) throws Exception {
        final Project project = findOneById(id);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByName(String name) throws Exception {
        final Project project = findOneByName(name);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByIndex(Integer index) throws Exception {
        final Project project = findOneByIndex(index);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project updateProjectStatusById(String id, Status status) throws Exception {
        final Project project = findOneById(id);
        if (project == null) throw new NullProjectException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project updateProjectStatusByName(String name, Status status) throws Exception {
        final Project project = findOneByName(name);
        if (project == null) throw new NullProjectException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project updateProjectStatusByIndex(Integer index, Status status) throws Exception {
        final Project project = findOneByIndex(index);
        if (project == null) throw new NullProjectException();
        project.setStatus(status);
        return project;
    }

    public void showProject(Project project) {
        if (project == null) return;
        System.out.println("[ID] " + project.getId());
        System.out.println("[NAME] " + project.getName());
        System.out.println("[DESCRIPTION] " + project.getDescription());
    }

}
