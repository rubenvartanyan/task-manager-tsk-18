package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.repository.ICommandRepository;
import ru.vartanyan.tm.api.service.ICommandService;
import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.model.Command;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommand();
    }

    @Override
    public Collection<String> getListArgumentName() {
        return commandRepository.getCommandsArgs();
    }

    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandNames();
    }

    @Override
    public void add(AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
