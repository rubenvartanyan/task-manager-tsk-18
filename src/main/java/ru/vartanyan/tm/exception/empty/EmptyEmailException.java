package ru.vartanyan.tm.exception.empty;

public class EmptyEmailException extends Exception{

    public EmptyEmailException() throws Exception {
        super("Error! Email cannot be null or empty...");
    }

}
